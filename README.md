Douglas Crockford's parseq
==========================

This package provides Douglas Crockford's public-domain [parseq][RQ]
library as a Node.js package. The package version indicates the upstream
»edition«.

Both a universal CommonJS/AMD module and an ECMAScript 6 module are
provided.

Notable features of this package are:
  * its major version number is equal to the parseq »edition«
  * it is automatically updated when a new edition of parseq is released
  * it contains unaltered parseq source code
  * the code is JSLint-clean


Versioning
----------

The major version number of this package indicates the parseq »edition« it
contains. Multiple updates to parseq on a single day are represented by
increasing minor version numbers. The patch version indicates updates to
this package itself.


Usage from Node.js
------------------

This package can be installed using

    $ npm install @jkuebart/parseq

For more detailed examples, see the [website][RQ].

```javascript
const parseq = require("@jkuebart/parseq");

parseq.sequence(
    requestor_array,
    time_limit
)
```


Usage from HTML
---------------

This is the easiest way to include parseq into a website:

```html
<script type="module">
    import parseq from "//unpkg.com/@jkuebart/parseq/dist/parseq.min.js";

    // …
</script>
```

If you're having to support browsers which can't handle ES6 modules, you
can simply load the universal module which provides a global variable
`parseq`:

```html
<script src="//unpkg.com/@jkuebart/parseq/dist/parseq.umd.min.js"></script>
```

Alternatively, CommonJS and AMD compatible loaders can also load the
universal module.


Updating
--------

A shell script is provided for building new packages when this project or
the upstream project is updated. It can be run using

    npm run editions

This creates branches and tags based on the »edition« of the upstream
project. Packages still need to be generated and published manually.

The local branches and tags can be viewed using

    npm run show-branches
    npm run show-tags

This can be used to automate some tasks, for example:

    npm run show-branches --silent |
    while read b
    do
        git push --set-upstream origin "${b#refs/heads/}:${b#refs/heads/}"
    done

or

    npm run show-tags --silent |
    while read t
    do
        git checkout "${t#refs/tags/}"
        npm publish --access public
    done

To easily remove automatically created local branches and tags, use

    npm run reset

There is also a shell script that determines whether the upstream project
has new commits.

    npm run show-branches --silent |
    npm run uptodate --silent


[RQ]: http://github.com/douglascrockford/parseq/
