#!/bin/sh -efu

if [ "$#" -lt 1 ]
then
    <<EOF cat 2>&1
usage: ${0##*/} submodule [files...]

    Read refs from standard input and check whether the given submodule has
    new commits which aren't in any of those refs.

    If files are given, only updates to those files are considered.

    Exit 0 if the submodule is up to date, 1 otherwise.
EOF
    exit 64 # EX_USAGE
fi

submodule="$1"
shift

# Stop if at least one ref contains the latest changes to relevant files.
while read ref
do
    current_sha1=$(git ls-tree "$ref" "$submodule" | awk '{ print $3 }')
    new_commits=$(
        git \
            -C "$submodule" \
            rev-list --count \
            origin/master --not "$current_sha1" \
            -- "$@"
    )
    if [ 0 -eq "$new_commits" ]
    then
        echo "$submodule: $ref is up to date ($current_sha1)."
        exit 0
    fi
done

echo "$submodule: out of date."
exit 1
