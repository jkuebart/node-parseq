/*property
    apply, assign, concat, create, file, format, freeze, input, lastIndexOf,
    map, name, output, plugins, prototype, reduce, sourcemap, substring
*/

import buble from "@rollup/plugin-buble";
import {terser} from "rollup-plugin-terser";

// Clone an object and add the given properties.

const extend = (
    object,
    properties
) => Object.assign(Object.create(object), properties);

// Flatten one level of arrays.

const flatMap = (values, f) => Array.prototype.concat.apply([], values.map(f));

// Add a component to the file name before the extension.

function withTag(filename, tag) {
    const extensionIndex = 1 + filename.lastIndexOf(".");
    return (
        filename.substring(0, extensionIndex) +
        `${tag}.` +
        filename.substring(extensionIndex)
    );
}

const formats = [

// Create both an ES6 and a universal module.

    (output) => [
        extend(output, {output: extend(output.output, {format: "es"})}),

        extend(
            output,
            {
                output: extend(
                    output.output,
                    {
                        file: withTag(output.output.file, "umd"),
                        format: "umd"
                    }
                ),
                plugins: output.plugins.concat([buble()])
            }
        )
    ],

// Generate an unmodified and a minified version.

    (output) => [
        output,

        extend(
            output,
            {
                output: extend(
                    output.output,
                    {file: withTag(output.output.file, "min")}
                ),
                plugins: output.plugins.concat([terser()])
            }
        )
    ]

];

export default Object.freeze(formats.reduce(

    (outputs, format) => flatMap(outputs, format),

    [{
        input: "parseq/parseq.js",
        output: {
            file: "dist/parseq.js",
            name: "parseq",
            sourcemap: true
        },
        plugins: []
    }]

));
